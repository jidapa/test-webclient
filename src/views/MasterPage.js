import React from 'react';
import HeaderNavbar from '../components/HeaderNavbar/index';
import Footer from '../components/Footer/index';
import SearchingMenu from '../components/SearchingMenu/index';
import DetailAlbums from '../components/DetailAlbums/index';
import '../views/style.css';
import { Container } from 'react-bootstrap';

class MasterPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      album: {}
    };
    this.callbackFunction = this.callbackFunction.bind(this);
  }

  callbackFunction = (dataFromChild) => {
    this.setState({ album: dataFromChild });
  }

  render() {
    return (
      <div id="master-page">
        <HeaderNavbar />
        <div className="body-content">
          <Container>
            <SearchingMenu parentCallback={this.callbackFunction} />
            <DetailAlbums setAlbum={this.state.album} />
          </Container>
        </div>
        <Footer />
      </div >
    )
  }
}

export default MasterPage;