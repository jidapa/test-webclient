import React from 'react';
import { Router, Route } from 'react-router';

import MasterPage from '../views/MasterPage';
import NotFound from '../components/NotFound/NotFound';

const Routes = (props) => (
  <Router {...props}>
    <Route path="/" component={MasterPage} />
    <Route path="*" component={NotFound} />
  </Router>
);

export default Routes;