import React, { Component } from 'react';
import '../DetailAlbums/style.scss';
import { Card, Container, Row, Col } from 'react-bootstrap';
class DetailAlbums extends Component {
    render() {
        return (
            <div id="detail-albums">
                {this.props.setAlbum.id !== undefined ?
                    <Card body>
                        <Container>
                            <Row>
                                <Col md={12}>
                                    <Row>
                                        <Col md={3} className="text-label">
                                            User ID :
                                    </Col>
                                        <Col md={9} className="text-content">
                                            {this.props.setAlbum.userId}
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={3} className="text-label">
                                            Album ID :
                                    </Col>
                                        <Col md={9} className="text-content">
                                            {this.props.setAlbum.id}
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={3} className="text-label">
                                            Title
                                    </Col>
                                        <Col md={9} className="text-content">
                                            {this.props.setAlbum.title}
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                        </Container>
                    </Card>
                    :
                    null
                }
            </div>
        )
    }
}

export default DetailAlbums;