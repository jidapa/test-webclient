import React, { Component } from 'react';
import '../SearchingMenu/style.scss';
import { Card, Form, Button, Row, Col } from 'react-bootstrap';
import axios from 'axios';

class SearchingMenu extends Component {
    constructor(props) {
        super(props);
        this.state = {
            albumId: "",
            album: {}
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
        this.setState({ albumId: event.target.value });
    }

    handleSubmit() {
        this.searchingAlbum(this.state.albumId);
    }

    searchingAlbum(albumId) {
        // fetch('https://jsonplaceholder.typicode.com/albums/' + albumId)
        //     .then(response => response.json())
        //     .then(data => this.props.parentCallback(data));
        axios.get('https://jsonplaceholder.typicode.com/albums/' + albumId)
            .then(res => {
                this.props.parentCallback(res.data)
            })
    }

    render() {
        return (
            <div id="searching-menu">
                <Card>
                    <Card.Body>
                        <Form>
                            <Row className="justify-content-md-center">
                                <Col xs lg="10">
                                    <Form.Control value={this.state.albumsId} onChange={this.handleChange} type="text" placeholder="Enter Albums ID" />
                                </Col>
                                <Col xs lg="2">
                                    <Button type="button" onClick={this.handleSubmit} id="albumsId" variant="primary" className="btn-default">
                                        Search
                                    </Button>
                                </Col>
                            </Row>
                        </Form>
                    </Card.Body>
                </Card>
            </div>
        )
    }
}

export default SearchingMenu;